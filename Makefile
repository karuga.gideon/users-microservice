up: 
	# mvn clean package
	docker build --build-arg JAR_FILE=target/*.jar -t gidemn/userservice .

run: 
	docker run -d -p 8080:8080 gidemn/userservice

bash:
	docker run -ti --entrypoint /bin/sh gidemn/userservice

