## About

This is a springboot microservice to save and retrieve users from a postgres db.

## Prerequisite

1. Java JDK installation

2. maven installation

3. postgres installation

## Create executable jar file

mvn clean package

## Build docker image

make up

## Rund docker image

make run
