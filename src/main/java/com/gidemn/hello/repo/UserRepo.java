package com.gidemn.hello.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.gidemn.hello.entity.User;

public interface UserRepo extends JpaRepository<User, Integer> {

    User findByEmail(String email);
}
