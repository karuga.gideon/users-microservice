package com.gidemn.hello.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.gidemn.hello.entity.User;
import com.gidemn.hello.repo.UserRepo;

@Service
public class UserService {

    @Autowired
    private UserRepo userRepo;

    public User getByID(int id) {
        return userRepo.findById(id).get();
    }

    public User findByEmail(String email) {
        return userRepo.findByEmail(email);
    }

    public List<User> filterUsers() {
        return userRepo.findAll();
    }

    public User save(User user) {
        return userRepo.save(user);
    }

    public User updateUser(User user, int userID) {

        User existingUser = userRepo.findById(userID).get();
        if (existingUser == null) {
            return null;
        }

        if (existingUser.getEmail() == null) {
            existingUser.setEmail(user.getEmail());
        }

        existingUser.setFirstName(user.getFirstName());
        existingUser.setLastName(user.getLastName());

        return userRepo.save(existingUser);
    }

}
