package com.gidemn.hello.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gidemn.hello.entity.User;
import com.gidemn.hello.service.UserService;

@RestController
@RequestMapping(value = "/users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/create")
    public User createUser(@RequestBody User user) {
        return userService.save(user);
    }

    @GetMapping("/{id}")
    public User findUserByID(@PathVariable int id) {
        return userService.getByID(id);
    }

    @GetMapping("/by_email/{email}")
    public User findUserByEmail(@PathVariable String email) {
        return userService.findByEmail(email);
    }

    @GetMapping("/filter")
    public List<User> filterUsers() {
        return userService.filterUsers();
    }

    @PutMapping("/update/{userID}")
    public User updateUser(@RequestBody User user, @PathVariable int userID) {
        return userService.updateUser(user, userID);
    }
}
